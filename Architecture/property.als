/**
* Property Module
*
* Property abstraction  for Component Base Architecture
* and Message Passing Asynchronous based communication model
*/
module stride

open cbsemetamodel
open connectorMPS


// Abstract property on component 
abstract sig ComponentProperty {
	comp: one Component,
	payl: one Payload
}

fun ComponentProperty.holds[c1:one Component,p: one Payload]: set ComponentProperty {
	{ m:this {	 m.comp = c1 and m.payl = p }}
}

// Abstract property representing a threat on communication between two component using MPS communication
abstract sig ConnectorProperty {
	conn: one Connector,
	payl: one Payload
}{ comp1 != comp2 }

fun ConnectorProperty.comp1 : one Component {
	{ c:Component | this.conn.portI in c.uses}
}

fun ConnectorProperty.comp2 : one Component {
	{ c:Component | this.conn.portO in c.uses}
}

fun ConnectorProperty.holds[c1,c2:one Component,p: one Payload]: set ConnectorProperty {
	{ m:this { m.comp1 = c1 and m.comp2 = c2 and m.payl = p }}
}
